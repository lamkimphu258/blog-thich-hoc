<?php

namespace App\Filament\Resources;

use App\Filament\Resources\CourseResource\Pages;
use App\Models\Course;
use Filament\Forms\Components\Checkbox;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Columns\IconColumn;
use Filament\Tables\Columns\ImageColumn;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class CourseResource extends Resource
{
    protected static ?string $model = Course::class;

    protected static ?string $navigationIcon = 'heroicon-o-collection';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                TextInput::make('title')
                    ->maxLength(255)
                    ->required(),
                TextInput::make('description')
                    ->maxLength(255)
                    ->required(),
                FileUpload::make('thumbnail')
                    ->directory('course_images')
                    ->required()
                    ->imageResizeTargetWidth('300')
                    ->imageResizeTargetHeight('250')
                    ->maxSize(5120),
                Checkbox::make('published'),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('title')
                    ->limit(100),
                TextColumn::make('description')
                    ->limit(100),
                ImageColumn::make('thumbnail'),
                IconColumn::make('published')
                    ->boolean()
                    ->sortable(),
            ])
            ->filters([
                Filter::make('title_filter')
                    ->label('Title')
                    ->form([
                        TextInput::make('title')
                    ])
                    ->query(fn (Builder $query, array $data): Builder => $query->where('title', 'like', $data['title'] . '%')),
                Filter::make('description_filter')
                    ->label('Description')
                    ->form([
                        TextInput::make('description')
                    ])
                    ->query(fn (Builder $query, array $data): Builder => $query->where('description', 'like', $data['description'] . '%')),
                Filter::make('published')
                    ->label('Published')
                    ->query(fn (Builder $query): Builder => $query->where('published', true))
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListCourses::route('/'),
            'create' => Pages\CreateCourse::route('/create'),
            'edit' => Pages\EditCourse::route('/{record}/edit'),
        ];
    }
}
