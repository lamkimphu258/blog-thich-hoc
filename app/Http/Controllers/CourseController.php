<?php

namespace App\Http\Controllers;

use App\Models\Course;

class CourseController extends Controller
{
  public function index()
  {
    return view('course.index');
  }

  public function show(string $id)
  {
    return view('course.show', [
      'course' => Course::findOrFail($id),
    ]);
  }
}
