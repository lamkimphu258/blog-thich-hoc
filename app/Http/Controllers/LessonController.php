<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Lesson;
use Illuminate\Http\Request;

class LessonController extends Controller
{
  public function show(
    string $courseId,
    string $lessonId
  ) {
    $course = Course::findOrFail($courseId);
    $nextLesson = $course->lessons()->select('id')->where('id', '>', $lessonId)->first();
    $previousLesson = $course->lessons()->select('id')->where('id', '<', $lessonId)->first();

    return view('lesson.show', [
      'course' => $course,
      'lesson' => Lesson::findOrFail($lessonId),
      'nextLessonId' => $nextLesson,
      'previousLessonId' => $previousLesson
    ]);
  }
}
