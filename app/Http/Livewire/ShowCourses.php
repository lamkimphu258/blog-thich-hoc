<?php

namespace App\Http\Livewire;

use App\Models\Course;
use Livewire\Component;
use Livewire\WithPagination;

class ShowCourses extends Component
{
    use WithPagination;

    public function render()
    {
        return view('livewire.course.show-courses', [
            'courses' => Course::latest()->paginate(12),
        ]);
    }
}
