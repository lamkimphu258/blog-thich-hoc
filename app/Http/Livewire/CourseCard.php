<?php

namespace App\Http\Livewire;

use Livewire\Component;

class CourseCard extends Component
{
    public function __construct(
        public $course
    ) {
    }

    public function render()
    {
        return view('livewire.course.course-card');
    }
}
