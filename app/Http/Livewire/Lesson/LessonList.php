<?php

namespace App\Http\Livewire\Lesson;

use App\Models\Course;
use Livewire\Component;

class LessonList extends Component
{
    public function __construct(
        public $course
    ) {
    }

    public function render()
    {
        return view(
            'livewire.lesson.lesson-list',
            ['lessons' => $this->course->lessons]
        );
    }
}
