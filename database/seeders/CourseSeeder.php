<?php

namespace Database\Seeders;

use App\Models\Course;
use App\Models\Lesson;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Schema::disableForeignKeyConstraints();
        Lesson::truncate();
        Course::truncate();
        Schema::enableForeignKeyConstraints();

        Course::factory()
            ->count(10)
            ->has(Lesson::factory()->count(3)->published())
            ->hasLessons(2)
            ->create();

        Course::factory()
            ->count(30)
            ->published()
            ->has(Lesson::factory()->count(2)->published())
            ->hasLessons(3)
            ->create();
    }
}
