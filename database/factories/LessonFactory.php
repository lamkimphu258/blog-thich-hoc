<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Lesson>
 */
class LessonFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => fake()->realTextBetween(10, 50),
            'content' => fake()->realTextBetween(1_000, 10_000),
        ];
    }

    public function published(): Factory
    {
        return $this->state(function () {
            return [
                'published' => true,
            ];
        });
    }
}
