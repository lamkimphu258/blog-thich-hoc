<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Course>
 */
class CourseFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $url = '/course_images/thumbnail.png';
        return [
            'title' => fake()->realText(),
            'description' => fake()->realText(),
            'thumbnail' => $url,
        ];
    }

    public function published(): Factory
    {
        return $this->state(function () {
            return [
                'published' => true,
            ];
        });
    }
}
