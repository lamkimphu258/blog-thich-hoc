import { defineConfig } from "vite";
import laravel from "laravel-vite-plugin";

export default defineConfig({
    plugins: [
        laravel({
            input: [
                "resources/css/app.css",
                "resources/css/courseIndex.css",
                "resources/css/courseShow.css",
                "resources/css/lessonShow.css",
                "resources/css/welcome.css",
                "resources/js/app.js",
            ],
            refresh: true,
        }),
    ],
});
