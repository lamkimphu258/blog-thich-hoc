<!DOCTYPE html>

<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Education website to help your life better">
  <meta name="keywords" content="HTML, CSS, JavaScript, Typescript, PHP, Thich Hoc, Lam Kim Phu">
  <meta name="author" content="Lam Kim Phu">

  @vite('resources/css/app.css')
  @livewireStyles

  <title>{{ $title ?? 'Thich Hoc' }}</title>
  {{ $customStyles ?? '' }}
</head>

<body>
  <header class="header">
    <x-navbar />
  </header>
  <main>{{ $slot }}</main>
  <footer>&copy; Thích Học {{ now()->year }}</footer>

  @livewireScripts
</body>

</html>
