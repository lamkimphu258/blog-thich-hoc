<nav class="navbar">
  <a class="navbar-brand" href="/" alt="Nav brand link">
    <x-logo :width='30' :height='30' />
    <span class="navbar-brand-text">Thích Học</span>
  </a>
  <ul class="navbar-nav">
    <li class="nav-item">
      <a href="/courses" alt="Courses Khoá học">Khoá học</a>
    </li>
    <li class="nav-item">
      <a href="/learning-paths" alt="Link Lộ trình">Lộ trình</a>
    </li>
  </ul>
</nav>
