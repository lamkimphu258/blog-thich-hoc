<x-layout>
    <x-slot:title>
        Trang chủ - Thích Học
        </x-slot>

        <x-slot:customStyles>
            @vite('resources/css/welcome.css')
        </x-slot:customStyles>

        <div class="introduction">
            <span class="introduction-text">học tập chất lượng và hoàn toàn miễn phí</span>
            <picture>
                <source srcset="{{ Vite::asset('resources/images/introduction.avif') }}" type="image/avif" />
                <source srcset="{{ Vite::asset('resources/images/introduction.webp') }}" type="image/webp" />
                <img class="introduction-image" src="{{ Vite::asset('resources/images/introduction.png') }}" alt="Banner" />
            </picture>
        </div>
</x-layout>
