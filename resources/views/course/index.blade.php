<x-layout>
  <x-slot:title>
    Khoá học - Thích Học
    </x-slot>

    <x-slot:customStyles>
      @vite('resources/css/courseIndex.css')
    </x-slot:customStyles>

    <livewire:show-courses />
</x-layout>
