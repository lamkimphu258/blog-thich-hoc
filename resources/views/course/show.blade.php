<x-layout>
  <x-slot:title>
    Khoá học chi tiết - Thích Học
    </x-slot>

    <x-slot:customStyles>
      @vite('resources/css/courseShow.css')
    </x-slot:customStyles>

    <div class="course">
      <h1>{{ $course->title }}</h1>
      <hr>
      <livewire:lesson.lesson-list :course="$course" />
    </div>
</x-layout>
