<x-layout>
  <x-slot:title>
    Bài học - Thích Học
    </x-slot>

    <x-slot:customStyles>
      @vite('resources/css/lessonShow.css')
    </x-slot:customStyles>

    <div class="lesson">
      <h1 class="lesson-title">{{ $lesson->title }}</h1>
      <hr>
      <div class="lesson-content">
        {!! $lesson->content !!}
      </div>

      <div @class([ 'flex w-full items-center mt-5' , 'justify-between'=> !empty($previousLessonId),
        'justify-end' => empty($previousLessonId)
        ])>
        @isset($previousLessonId)
        <button class="btnPrimary">
          <a href={{ route('lesson-show', ['courseId' => $course->id, 'lessonId' => $previousLessonId]) }} alt="Previous Lesson">
            < Bài trước</a>
        </button>
        @endisset
        @isset($nextLessonId)
        <button class="btnPrimary">
          <a href={{ route('lesson-show', ['courseId' => $course->id, 'lessonId' => $nextLessonId]) }} alt="Next Lesson">Bài tiếp theo ></a>
        </button>
        @endisset
      </div>
    </div>
</x-layout>
