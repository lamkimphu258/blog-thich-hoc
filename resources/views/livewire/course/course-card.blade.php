<div class="w-full rounded-xl overflow-hidden shadow-lg bg-white cursor-pointer hover:scale-105 transition duration-500 h-[450px]" :wire:key="course-card-{{$course->id}}">
    <img class="w-full h-auto min-h-[250px] max-h-[300px]" src="{{ asset('storage/' . $course->thumbnail ) }}" alt="Course Thumbnail" />
    <div class="px-6 py-4 min-h-[250px]">
        <div class="font-bold text-xl mb-2">{{ Str::limit($course->title, 100) }}</div>
        <p class="text-gray-700 text-base">
            {{ Str::limit($course->description, 100) }}
        </p>
    </div>
</div>
