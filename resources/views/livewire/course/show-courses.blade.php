<div>
  <div class="course-list">
    @foreach($courses as $course)
    <a href="/courses/{{ $course->id }}" alt="Course link">
      <livewire:course-card :course="$course" wire:key="course-{{ $course->id }}" />
    </a>
    @endforeach
  </div>

  {{ $courses->links() }}
</div>
