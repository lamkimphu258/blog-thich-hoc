<div class="mt-5">
    <ul>
        @foreach($lessons as $index=>$lesson)
        <a href={{ route('lesson-show', ['courseId' => $course->id, 'lessonId' => $lesson->id]) }} alt="lesson detail">
            <li class="lesson" wire:key="lesson-{{ $lesson->id }}">{{ $index+1 }}. {{ $lesson->title }}</li>
        </a>
        @endforeach
    </ul>
    <button class="btnPrimary">
        <a href={{ route('course-index') }} alt='Courses link'>Xem tất cả khoá học</a>
    </button>
</div>
