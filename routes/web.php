<?php

use App\Http\Controllers\CourseController;
use App\Http\Controllers\LessonController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::controller(CourseController::class)->group(function () {
    Route::get('/courses', 'index')
        ->name('course-index');
    Route::get('/courses/{id}', 'show')
        ->name('course-show');
});

Route::controller(LessonController::class)->group(function () {
    Route::get('/courses/{courseId}/lessons/{lessonId}', 'show')
        ->name('lesson-show');
});

Route::get('/learning-paths', function () {
    return view('learning-path');
});
